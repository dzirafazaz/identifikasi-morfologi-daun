-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Sep 2023 pada 11.17
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `class_daun`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_commen`
--

CREATE TABLE `tbl_commen` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_commen`
--

INSERT INTO `tbl_commen` (`id`, `name`, `email`, `comment`) VALUES
(1, 'Quentin', 'lorem@icloud.org', 'Aplikasi identifikasi daun antiinflamasi ini sangat membantu dalam memahami manfaat tanaman herbal dan pengobatan alami.'),
(2, 'Beatrice', 'nunc.risus@google.net', 'Sangat mudah digunakan dan informatif. Aplikasi ini memberikan panduan yang jelas tentang berbagai jenis daun antiinflamasi.'),
(3, 'Kirsten', 'eget.magna.suspendisse@hotmail.com', 'Saya sangat senang dengan tampilan antarmuka yang bersih dan intuitif. Sangat mudah untuk menavigasinya.'),
(4, 'Amos', 'suspendisse.sagittis.nullam@yahoo.couk', 'Aplikasi ini telah meningkatkan pemahaman saya tentang cara menggunakan tanaman herbal untuk meredakan peradangan. Sangat berguna!'),
(5, 'Tana', 'et.tristique@google.com', 'Saya suka bagaimana aplikasi ini memberikan informasi detail tentang daun-daun tertentu, termasuk cara penggunaannya.'),
(6, 'Xantha', 'adipiscing@google.net', 'Fitur pencarian yang kuat memungkinkan saya dengan cepat menemukan daun yang saya butuhkan. Sangat praktis!'),
(7, 'Alan', 'donec.porttitor@icloud.edu', 'Ulasan pengguna dan komentar positif dari orang lain memberikan pandangan yang berharga tentang efektivitas tanaman herbal tertentu.'),
(8, 'Brynn', 'penatibus.et.magnis@aol.ca', 'Aplikasi ini memberikan informasi yang terpercaya dan akurat tentang penggunaan tanaman herbal dalam meredakan peradangan.'),
(9, 'Faith', 'pharetra.sed@outlook.ca', 'Saya telah mencoba beberapa resep yang disarankan oleh aplikasi ini, dan hasilnya sangat memuaskan.'),
(10, 'Alden', 'magna.praesent@protonmail.ca', 'Terima kasih kepada tim pengembang aplikasi ini karena telah memberikan sumber daya yang sangat bermanfaat untuk perawatan kesehatan alami.'),
(11, 'admin', 'admin@gmail.com', 'tes komen');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `email` varchar(80) NOT NULL,
  `password` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `name`, `username`, `email`, `password`) VALUES
(2, 'user', 'user', 'user@gmail.com', '9b6a3872b286ee8ae5ed65f68c21e072f421c2cb'),
(3, 'admin', 'admin', 'admin@gmail.com', '9b6a3872b286ee8ae5ed65f68c21e072f421c2cb'),
(5, 'Quentin', 'Quentin', 'lorem@icloud.org', '9b6a3872b286ee8ae5ed65f68c21e072f421c2cb'),
(6, 'Beatrice', 'Beatrice', 'nunc.risus@google.net', '9b6a3872b286ee8ae5ed65f68c21e072f421c2cb'),
(7, 'Kirsten', 'Kirsten', 'eget.magna.suspendisse@hotmail.com', '9b6a3872b286ee8ae5ed65f68c21e072f421c2cb'),
(8, 'Amos', 'Amos', 'suspendisse.sagittis.nullam@yahoo.couk', '9b6a3872b286ee8ae5ed65f68c21e072f421c2cb'),
(9, 'Tana', 'Tana', 'et.tristique@google.com', '9b6a3872b286ee8ae5ed65f68c21e072f421c2cb'),
(10, 'Xantha', 'Xantha', 'adipiscing@google.net', '9b6a3872b286ee8ae5ed65f68c21e072f421c2cb'),
(11, 'Alan', 'Alan', 'donec.porttitor@icloud.edu', '9b6a3872b286ee8ae5ed65f68c21e072f421c2cb'),
(12, 'Brynn', 'Brynn', 'penatibus.et.magnis@aol.ca', '9b6a3872b286ee8ae5ed65f68c21e072f421c2cb'),
(13, 'Faith', 'Faith', 'pharetra.sed@outlook.ca', '9b6a3872b286ee8ae5ed65f68c21e072f421c2cb'),
(14, 'Alden', 'Alden', 'magna.praesent@protonmail.ca', '9b6a3872b286ee8ae5ed65f68c21e072f421c2cb');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_commen`
--
ALTER TABLE `tbl_commen`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_commen`
--
ALTER TABLE `tbl_commen`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
