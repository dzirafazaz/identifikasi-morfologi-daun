from flask import Flask, render_template, request, redirect, url_for, session, jsonify
from flask_mysqldb import MySQL
import MySQLdb.cursors
import re
import hashlib
from fastai.vision.all import *
import json
import pathlib
temp = pathlib.PosixPath
pathlib.PosixPath = pathlib.WindowsPath


app = Flask(__name__)
app.secret_key = 'daun'
# app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_HOST'] = 'dzirax.mysql.pythonanywhere-services.com'
# app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_USER'] = 'dzirax'
app.config['MYSQL_PASSWORD'] = 'beta4tester'
app.config['MYSQL_DB'] = 'class_daun'

# Intialize MySQL
mysql = MySQL(app)

model_path = Path('model_daun_terbaru.pkl')
learn = load_learner(model_path)


@app.route('/')
def home():
    if 'loggedin' in session:
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor1 = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM tbl_user WHERE id = %s',
                       (session['id'],))
        account = cursor.fetchone()
        cursor1.execute('SELECT * FROM tbl_commen')
        comment = cursor1.fetchall()
        return render_template('index.html', account=account, comment=comment)
    return redirect(url_for('login'))


@app.route('/login/', methods=['GET', 'POST'])
def login():
    msg = ''
    if 'loggedin' in session:
        return redirect(url_for('index'))
        # Check if "username" and "password" POST requests exist (user submitted form)
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
        # Create variables for easy access
        username = request.form['username']
        password = request.form['password']
        # Retrieve the hashed password
        hash = password + app.secret_key
        hash = hashlib.sha1(hash.encode())
        password = hash.hexdigest()
        # Check if account exists using MySQL
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute(
            'SELECT * FROM tbl_user WHERE username = %s AND password = %s', (username, password,))
        # Fetch one record and return the result
        account = cursor.fetchone()
        # If account exists in accounts table in out database
        if account:
            # Create session data, we can access this data in other routes
            session['loggedin'] = True
            session['id'] = account['id']
            session['username'] = account['username']
            # Redirect to home page
            return redirect(url_for('index'))
        else:
            # Account doesnt exist or username/password incorrect
            msg = 'Incorrect username/password!'
    # Output message if something goes wrong...
    return render_template('login.html', msg=msg)

# http://localhost:5000/pythinlogin/register - this will be the registration page, we need to use both GET and POST requests


@app.route('/register', methods=['GET', 'POST'])
def register():
    if 'loggedin' in session:
        return redirect(url_for('index'))
    # Output message if something goes wrong...
    msg = ''
    # Check if "username", "password" and "email" POST requests exist (user submitted form)
    if request.method == 'POST' and 'name' in request.form and 'username' in request.form and 'password' in request.form and 'email' in request.form:
        # Create variables for easy access
        name = request.form['name']
        username = request.form['username']
        password = request.form['password']
        email = request.form['email']
        # Check if account exists using MySQL
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute(
            'SELECT * FROM tbl_user WHERE username = %s', (username,))
        account = cursor.fetchone()
        # If account exists show error and validation checks
        if account:
            msg = 'Account already exists!'
        elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
            msg = 'Invalid email address!'
        elif not re.match(r'[A-Za-z0-9]+', username):
            msg = 'Username must contain only characters and numbers!'
        elif not username or not password or not email:
            msg = 'Please fill out the form!'
        else:
            # Hash the password
            hash = password + app.secret_key
            hash = hashlib.sha1(hash.encode())
            password = hash.hexdigest()
            # Account doesn't exist, and the form data is valid, so insert the new account into the accounts table
            cursor.execute('INSERT INTO tbl_user VALUES (%s, %s, %s, %s, %s)',
                           ('', name, username, email, password))
            mysql.connection.commit()
            msg = 'You have successfully registered!'
    elif request.method == 'POST':
        # Form is empty... (no POST data)
        msg = 'Please fill out the form!'
    # Show registration form with message (if any)
    return render_template('register.html', msg=msg)

# http://localhost:5000/python/logout - this will be the logout page


@app.route('/logout')
def logout():
    # Remove session data, this will log the user out
    session.pop('loggedin', None)
    session.pop('id', None)
    session.pop('username', None)
    # Redirect to login page
    return redirect(url_for('login'))


@app.route('/identifikasi', methods=['GET', 'POST'])
def identifikasi():
    if request.method == 'POST':
        image = request.files['image']
        img = PILImage.create(image)
        pred_class, pred_idx, outputs = learn.predict(img)
        probabilities = sorted(
            list(zip(learn.dls.vocab, outputs)), key=lambda p: p[1], reverse=True)
        if pred_class != "Other" and probabilities[0][1] > 0.20:
            probabilities_str = json.dumps([(c, p.item() * 100) for c, p in probabilities])
            return render_template('identifikasi.html', prediction=pred_class, probabilities=probabilities, probabilities_str=probabilities_str)
    return render_template('identifikasi.html')





# @app.route('/profile')
# def profile():
#     # Check if the user is logged in
#     if 'loggedin' in session:
#         # We need all the account info for the user so we can display it on the profile page
#         cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
#         cursor.execute('SELECT * FROM tbl_user WHERE id = %s', (session['id'],))
#         account = cursor.fetchone()
#         # Show the profile page with account info
#         return render_template('profile.html', account=account)
#     # User is not logged in redirect to login page
#     return redirect(url_for('login'))


@app.route('/profile', methods=['GET', 'POST'])
def profile():
    if 'loggedin' in session:
        if request.method == 'POST':
            name = request.form['name']
            username = request.form['username']
            email = request.form['email']
            password = request.form['password']

            cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
            cursor.execute('UPDATE tbl_user SET name = %s, username = %s, password = %s, email = %s WHERE id = %s',
                           (name, username, password, email, session['id']))
            mysql.connection.commit()

            cursor.execute(
                'SELECT * FROM tbl_user WHERE id = %s', (session['id'],))
            updated_account = cursor.fetchone()

            return render_template('profile.html', account=updated_account, message='Profile updated successfully!')

        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM tbl_user WHERE id = %s',
                       (session['id'],))
        account = cursor.fetchone()

        return render_template('profile.html', account=account)

    return redirect(url_for('login'))


@app.route('/search')
def search():
    return render_template('search.html')


@app.route('/gallery')
def gallery():
    return render_template('gallery.html')


@app.route('/comments')
def comments():
    return render_template('comments.html')


@app.route('/physical_traits')
def physical_traits():
    return render_template('ciri.html')


@app.route('/herb_info')
def herb_info():
    return render_template('herb_info.html')


@app.route('/index', methods=['GET', 'POST'])
def index():
    msg = ''
    if 'loggedin' in session:
        if request.method == 'POST' and 'name' in request.form and 'email' in request.form and 'comment' in request.form:
            # Create variables for easy access
            name = request.form['name']
            comment = request.form['comment']
            email = request.form['email']
            # Check if account exists using MySQL
            cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
            cursor.execute('INSERT INTO tbl_commen VALUES (%s, %s, %s, %s)',
                           ('', name, email, comment))
            mysql.connection.commit()
            msg = 'Tersimpan!'
            return redirect(url_for('index'))
    # Show registration form with message (if any)
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor1 = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM tbl_user WHERE id = %s',
                       (session['id'],))
        account = cursor.fetchone()
        cursor1.execute('SELECT * FROM tbl_commen')
        comment = cursor1.fetchall()
        return render_template('index.html', account=account, comment=comment)
    return redirect(url_for('login'))


if __name__ == '__main__':
    app.run(debug=False,host='0.0.0.0')
